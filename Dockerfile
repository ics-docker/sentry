FROM sentry:9.1.2

LABEL maintainer="anders.harrisson@esss.se"

RUN apt-get update && \
    apt-get install --yes \
        libsasl2-dev \
        python-dev \
        libldap2-dev \
        libssl-dev && \
    pip install sentry-ldap-auth==2.7

COPY sentry.conf.py /etc/sentry/
