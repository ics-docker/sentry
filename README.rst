sentry
========

ESS specific Docker image for [Sentry](https://sentry.io/).

This image is based on the official repository image for Sentry:
https://hub.docker.com/r/library/sentry/

To publish a new image, the repository should be tagged.
Always use the tag of the original sentry image + '-ESSX' (increment X as required).
